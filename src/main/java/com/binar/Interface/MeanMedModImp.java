package com.binar.Interface;

import java.util.List;
import java.util.Map;

public interface MeanMedModImp {

    double hitungMean(List<Integer> list);
    double hitungMedian(List<Integer> numArray);
    int hitungModus (List<Integer> array);
    Map<Integer, Integer> freq(List<Integer> array);
}
