package com.binar.Service;

import com.binar.Interface.MainMenu;

import java.util.InputMismatchException;

public class MainMenuImp extends Menuku{
    public String csvDir = "src/main/resources/data_sekolah.csv";
    public String meanMedModDir = "src/main/resources/MeanMedMod_Sekolah.txt";
    public String kelompokDir = "src/main/resources/Pengelompokkan_Sekolah.txt";
    public String bothDir = "src/main/resources/File_gabungan.txt";

    @Override
    public void tampilMenu(){
        MainMenuImp mainMenuImp = new MainMenuImp();
        try{
            MeanMedMod file = new MeanMedMod();
            MenuSelesai selesai = new MenuSelesai();
            System.out.println("\n-----------------------------------------------------------");
            System.out.println("              Aplikasi Pengolah Nilai Siswa                ");
            System.out.println("-----------------------------------------------------------" +
                    "\nSilahkan masukkan pilhan Anda : " +
                    "\n1. Generate File txt Pengelompokkan Nilai" +
                    "\n2. Generate File txt Mean-Median-Modus" +
                    "\n3. Generate File txt Kedua File" +
                    "\n0. Exit");
            switch (promptInput()){
                case 0 :
                    System.out.println("Program sedang ditutup");
                    System.exit(0);
                    break;
                case 1 :
                    file.writeKelompok(kelompokDir);
                    selesai.tampilMenu();
                    break;
                case 2 :
                    file.writeMeanMedMod(meanMedModDir);
                    selesai.tampilMenu();
                    break;
                case 3 :
                    file.writeBoth(bothDir);
                    selesai.tampilMenu();
                    break;
                default:
                    System.out.println("Masukkan angka yang sesuai (0-3");
                    selesai.tampilMenu();
                    break;
            }
        }catch (InputMismatchException e){
            System.err.println("Masukkan inputan Integer!");
        }
        mainMenuImp.tampilMenu();
    }
}
