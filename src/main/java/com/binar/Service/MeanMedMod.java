package com.binar.Service;

import com.binar.Interface.MeanMedModImp;

import java.io.*;
import java.util.*;

public class MeanMedMod<var> implements MeanMedModImp {
    // Membaca file csv
    public <String>List<Integer> read(String path){
        try {
            File file = new File(java.lang.String.valueOf(path));
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);

            java.lang.String line = " ";
            java.lang.String[] tempArr;

            List<Integer> listInt = new ArrayList<>();

            while ((line = reader.readLine()) != null) {
                tempArr = line.split(";");

                for (int i = 0; i < tempArr.length; i++) {
                    if (i == 0) {

                    } else {
                        java.lang.String temp = tempArr[i];
                        int intTemp = Integer.parseInt(temp);
                        listInt.add(intTemp);
                    }

                }
            }
            reader.close();
            return listInt;

        } catch (IOException e) {
            System.err.println("File yang dibaca tidak ditemukan");
        }
        return null;
    }

    // Menulis file pengelompokkan nilai sekolah
    public void writeKelompok(String kelompokDir){

        MainMenuImp menu = new MainMenuImp();

        try {
            File file = new File(kelompokDir);
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);

            Map<Integer, Integer> hMap = freq(read(menu.csvDir));

            bwr.write("Berikut Hasil Pengolahan Nilai:");
            bwr.write("\n\n");
            bwr.write("| Nilai " + "|" + " Frekuensi "  + "|\n");
            bwr.write("|\t5\t"    + "|\t  " + hMap.get(5)  + " \t|\n");
            bwr.write("|\t6\t"    + "|\t  " + hMap.get(6)  + " \t|\n");
            bwr.write("|\t7\t"    + "|\t  " + hMap.get(7)  + " \t|\n");
            bwr.write("|\t8\t"    + "|\t  " + hMap.get(8)  + " \t|\n");
            bwr.write("|\t9\t"    + "|\t  " + hMap.get(9)  + " \t|\n");
            bwr.write("|\t10\t"   + "|\t  " + hMap.get(10) + " \t|\n");


            bwr.flush();
            bwr.close();

            System.out.println("File berhasil tertulis");
        } catch (IOException | InputMismatchException | NullPointerException | IndexOutOfBoundsException e) {
            System.err.println("Tidak dapat menulis file pengelompokkan data\n");
        }
    }

    // Menulis file menghitung mean, median, modus
    public void writeMeanMedMod(String meanMedModDir){

        MainMenuImp menu = new MainMenuImp();

        try {

            File file = new File(meanMedModDir);
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);

            bwr.write("Berikut Hasil Pengolahan Nilai: ");
            bwr.newLine();
            bwr.newLine();

            // Menghitung mean
            bwr.write("Mean   : " + String.format("%.2f", hitungMean(read(menu.csvDir))));
            bwr.newLine();

            // Menghitung median
            bwr.write("Median : " + hitungMedian(read(menu.csvDir)));
            bwr.newLine();

            // Menghitung modus
            bwr.write("Modus  : " + hitungModus(read(menu.csvDir)));
            bwr.newLine();

            bwr.flush();
            bwr.close();

            System.out.println("File berhasil tertulis");
        } catch (IOException | InputMismatchException | NullPointerException | IndexOutOfBoundsException e) {
            System.err.println("Tidak dapat menulis file perhitungan mean, modus dan median\n");
        }

    }

    // Menulis kedua file
    public void writeBoth (String bothDir){
        MainMenuImp menu = new MainMenuImp();

        try {
            File file = new File(bothDir);
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);

            bwr.write("Berikut Hasil Pengolahan Nilai:");
            bwr.write("\n\n");

            // Menghitung mean
            bwr.write("Mean   : " + String.format("%.2f", hitungMean(read(menu.csvDir))));
            bwr.newLine();

            // MEDIAN
            bwr.write("Median : " + hitungMedian(read(menu.csvDir)));
            bwr.newLine();

            // MODUS
            bwr.write("Modus  : " + hitungModus(read(menu.csvDir)));
            bwr.newLine();

            Map<Integer, Integer> hMap = freq(read(menu.csvDir));

            bwr.newLine();
            bwr.write("Berikut Hasil Pengolahan Nilai: ");
            bwr.write("\n\n");
            bwr.write("| Nilai " + "|" + " Frekuensi "  + "|\n");
            bwr.write("|\t5\t"    + "|\t  " + hMap.get(5)  + " \t|\n");
            bwr.write("|\t6\t"    + "|\t  " + hMap.get(6)  + " \t|\n");
            bwr.write("|\t7\t"    + "|\t  " + hMap.get(7)  + " \t|\n");
            bwr.write("|\t8\t"    + "|\t  " + hMap.get(8)  + " \t|\n");
            bwr.write("|\t9\t"    + "|\t  " + hMap.get(9)  + " \t|\n");
            bwr.write("|\t10\t"   + "|\t  " + hMap.get(10) + " \t|\n");

            bwr.flush();
            bwr.close();

            System.out.println("File berhasil tertulis");

        }catch (IOException | InputMismatchException | NullPointerException | IndexOutOfBoundsException e) {
            System.err.println("Tidak dapat menulis kedua file\n");
        }
    }

    @Override
    public double hitungMean(List<Integer> list) {
        return list.stream()
                .mapToDouble(d -> d)
                .average()
                .orElse(0.0);
    }

    @Override
    public double hitungMedian(List<Integer> numArray) {
        Arrays.sort(new List[]{numArray});
        double median;
        if (numArray.size() % 2 == 0)
            median = ((double) numArray.get(numArray.size() / 2) +
                    (double) numArray.get(numArray.size() / 2 - 1)) / 2;
        else
            median = (double) numArray.get(numArray.size() / 2);
        return median;
    }

    @Override
    public int hitungModus (List<Integer> array) {
        HashMap<Integer, Integer> hm = new HashMap<>();
        int max = 1;
        int temp = 0;

        for (Integer integer : array) {

            if (hm.get(integer) != null) {

                int count = hm.get(integer);
                count++;
                hm.put(integer, count);

                if (count > max) {
                    max = count;
                    temp = integer;
                }
            } else
                hm.put(integer, 1);
        }
        return temp;
    }

    @Override
    public Map<Integer, Integer> freq(List<Integer> array) {
        Set<Integer> distinct = new HashSet<>(array);
        Map<Integer, Integer> mMap = new HashMap<>();

        for (Integer s : distinct) {
            mMap.put(s, Collections.frequency(array, s));
        }
        return mMap;
    }
}
