package com.binar;

import com.binar.Service.MeanMedMod;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class MeanMedModTest {

    MeanMedMod meanMedMod;
    ArrayList arrayList, listNullArray = null;
    Map<Integer, Integer> angkaMap;
    String csvDir = "src/main/resources/data_sekolah.csv";
    String csvDirWrong = "ini/wrong/path/yak";
    String meanMedModDir = "src/main/resources/MeanMedMod_Sekolah.txt";
    String kelompokDir = "src/main/resources/Pengelompokkan_Sekolah.txt";
    String bothDir = "src/main/resources/File_gabungan.txt";


    @BeforeEach
    void setup(){
        meanMedMod = new MeanMedMod();
        listAngka();
        mapAngka();
    }
    void listAngka(){
        Integer[] arrList = {
                7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10,
                7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10,5,6,7,
                7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10,
                7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10,7,7,10,
                7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10,
                7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10,
                7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10,6,7,8,
                7,7,7,7,7,7,7, 8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10,7,7,9,9,8
        };
        arrayList = new ArrayList<>(Arrays.asList(arrList));
    }
    void mapAngka(){
        Integer [] keys = {5, 6, 7, 8, 9, 10};
        Integer [] values = {1, 2, 62, 42, 50, 41};
        angkaMap = IntStream.range(0, keys.length).boxed()
                .collect(Collectors.toMap(i->keys[i], i->values[i]));
    }

    @Test
    @DisplayName("Positif Test - Check hitungMean")
    void positifHitungMean(){
        double hasilHitungMean = 8.318181818181818;
        Assertions.assertEquals(hasilHitungMean, meanMedMod.hitungMean(arrayList));
    }

    @Test
    @DisplayName("Negatif Test - Check hitungMean")
    void negatifHitunganMean(){
        Assertions.assertThrows(NullPointerException.class, () -> meanMedMod.hitungMean(listNullArray));
    }

    @Test
    @DisplayName("Positif Test - Check hitungModus")
    void positifHitungModus(){
        int hasilHitungModus = 7;
        Assertions.assertEquals(hasilHitungModus, meanMedMod.hitungModus(arrayList));
    }
    @Test
    @DisplayName("Negatif Test - Check hitungModus")
    void negatifHitungModus(){
        Assertions.assertThrows(NullPointerException.class, () -> meanMedMod.hitungModus(listNullArray));
    }

    @Test
    @DisplayName("Positif Test - Check hitungMedian")
    void positifHitungMedian(){
        double hasilHitungMedian = 7;
        Assertions.assertEquals(hasilHitungMedian, meanMedMod.hitungMedian(arrayList));
    }

    @Test
    @DisplayName("Negatif Test - Check hitungMedian")
    void negatifHitungMedian(){
        Assertions.assertThrows(NullPointerException.class, () -> meanMedMod.hitungMedian(listNullArray));
    }

    @Test
    @DisplayName("Positif Test - Check Frekuensi")
    void positifFrekuensi(){
        Assertions.assertEquals(angkaMap, meanMedMod.freq(arrayList));
    }

    @Test
    @DisplayName("Negatif Test - Check Frekuensi")
    void negatifFrekuensi(){
        Assertions.assertThrows(NullPointerException.class, ()->meanMedMod.freq(listNullArray));
    }

    @Test
    @DisplayName("Positif Test - File berhasil terbaca di path yang benar dan isinya")
    void positifFileTerbaca(){
        fileDiBuat();
        fileSudahAda();
        fileTerbuatIsi();
    }
    void fileDiBuat(){
        Assertions.assertDoesNotThrow(()->meanMedMod.read(csvDir));
    }
    void fileSudahAda(){
        File file = new File(csvDir);
        Assertions.assertTrue(file.exists());
    }
    void fileTerbuatIsi(){
        Assertions.assertEquals(arrayList, meanMedMod.read(csvDir));
    }

    @Test
    @DisplayName("Negatif Test - File berada di path yang salah")
    void negatifFileTerbaca(){
//        Assertions.assertThrows(IOException.class, ()-> meanMedMod.read(csvDirWrong));
    }
}